# 基于RN.cn官网的学习案例

## 开发启动
1. react-native init AwesomeProject
2. iOS启动, react-native run-ios
3. Android启动, react-native run-android


命令行中React native项目目录下键入react-native run-ios会启动iOS模拟器，默认是使用iPhone6，如果想要试用其他版本的模拟器则需要在react-native run-ios后携带参数–simulator

启动iPhone8：
react-native run-ios --simulator "iPhone 8"

—-simulator后指定模拟器的名字

查看iOS设备：在终端中输入 xcrun simctl list devices

若已经打开了一个模拟器，需要先关闭这个模拟器，再执行react-native run-ios命令打开的就是新的模拟器



