/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// https://reactnative.cn/docs/components-and-apis/
// 基础组件
// View , Text , Image , TextInput , ScrollView , StyleSheet

// 交互控件
// Button , Picker(iOS, Android各自的选择器控件) ,  Slider(滑动数值选择器) , Switch (开关控件)

// 列表视图
// 只会渲染当前屏幕可见的元素, FlatList (高性能的滚动列表组件) , SectionList (类似FlatList, 但是多了分组显示)

// iOS独有的组件和API
// ActionSheetIOS , AlertIOS , DatePickerIOS , ImagePickerIOS , NavigatorIOS , ProgressViewIOS ,
// PushNotificationIOS , SegmentedControlIOS , TabBarIOS (和TabBarIOS.Item搭配使用) ,

// Android独有的组件和API
// BackHandler , DatePickerAndroid, DrawerLayoutAndroid , PermissionsAndroid (Android 6.0) ,
// ProgressBarAndroid , TimePickerAndroid , ToastAndroid , ToolbarAndroid , ViewPagerAndroid ,

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image, ScrollView } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

// 检查iOS版本
// const marjorVersonIOS = parseInt(Platform.Version, 10);
// if (marjorVersonIOS < 10) {
//   console.log('work around a change in behavior.');
// }

// 检查Android版本
// if (Platform.Version == 25) {
//   console.log('running on Nougat.');
// }

// 具体业务逻辑处理
// 电影数据数组
var MOCKED_MOVIES_DATA = [
  {
    title: "标题",
    year: "2015",
    poster: { thumbnail: "http://i.imgur.com/UePbdph.jpg" }
  }
];

type Props = {};

// 自定义组件 Blink, option + shift + F : format code ;
// option + shift + down : copy current line code to the next white line
// option + up : move the current code to up white line
class Blink extends Component<Props> {
  constructor(props) {
    // 1,
    super(props);
    this.state = { showText: true };

    // 2, 每1000毫秒对showText状态做一次取反操作
    setInterval(() => {
      this.setState(previousState => {
        return { showText: !previousState.showText };
      });
    }, 1000);
  } // end constructor

  // 3, 根据当前showText的值决定是否显示text内容
  render() {
    let display = this.state.showText ? this.props.text : "";
    return <Text style={styles.bigblue}>{display}</Text>;
  }
}

// 自定义组件 Greeting
class Greeting extends Component<Props> {
  render() {
    return <Text>Hello , {this.props.name}</Text>;
  }
}

export default class App extends Component<Props> {
  render() {
    var movie = MOCKED_MOVIES_DATA[0];
    let pic = {
      uri:
        "https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg"
    };
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>

        {/* <Image style={styles.thumbnail} source={{uri: movie.poster.thumbnail}} /> */}
        <Image
          style={{ width: 63, height: 91 }}
          source={{ uri: movie.poster.thumbnail }}
        />
        <Text style={styles.instructions}>{movie.title}</Text>
        <Text style={styles.instructions}>{movie.year}</Text>

        <Image source={pic} style={{ width: 193, height: 110 }} />

        <ScrollView>
          <Greeting name="kotori" />
          <Greeting name="nagisa" />
          <Greeting name="kotomi" />
          <Greeting name="hello" />
          <Greeting name="world" />
          <Greeting name="nihao" />
          <Greeting name="react" />
          <Greeting name="react-native" />
        </ScrollView>

        <Blink text="I love to blink" />
        <Blink text="Yes blinking is so great" />
        <Blink text="Why did they ever take this out of HTML" />
        <Blink text="Look at me look at me look at me" />
        <Blink text="I love to blink" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection, default value: column
    flexDirection: 'column',
    // flexDirection: 'row',
    // height: 300,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    backgroundColor: 'skyblue'
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 8
  },
  thumbnail: {
    width: 53,
    height: 81
  },
  bigblue: {
    color: "blue"
    // fontWeight: 'bold',
    // fontSize: 30,
  },
  red: {
    color: "red"
  }, 
});
